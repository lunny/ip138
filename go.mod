module gitea.com/lunny/ip138

go 1.12

require (
	github.com/stretchr/testify v1.3.0
	golang.org/x/net v0.0.0-20190603091049-60506f45cf65
	golang.org/x/text v0.3.2
)
