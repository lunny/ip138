package ip138

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIP(t *testing.T) {
	ip, err := ExternalIP()
	assert.NoError(t, err)
	fmt.Println(ip)
}
