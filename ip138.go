package ip138

import (
	"bytes"
	"errors"
	"io/ioutil"
	"net/http"
	"strings"

	"golang.org/x/net/html/charset"
	"golang.org/x/text/encoding"
	"golang.org/x/text/transform"
)

func detectEncoding(bytes []byte) (encoding.Encoding, error) {
	e, _, _ := charset.DetermineEncoding(bytes, "")
	return e, nil
}

const (
	url = "http://20019.ip138.com/ic.asp"
)

var (
	start = []byte("您的IP是：[")
	end   = []byte("]")
)

// ExternalIP returns external ip of current machine
func ExternalIP() (string, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}
	req.Header.Set("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3")
	//req.Header.Set("Accept-Encoding", "gzip, deflate")
	req.Header.Set("Accept-Language", "zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7")
	req.Header.Set("Cache-Control", "max-age=0")
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Cookie", "Hm_lvt_ecdd6f3afaa488ece3938bcdbb89e8da=1557366619; Hm_lpvt_ecdd6f3afaa488ece3938bcdbb89e8da=1557366619; pgv_pvi=5666361344; pgv_si=s6831747072; ASPSESSIONIDAASSRDDC=AOJEAEEBECJAEPDKMLEBBLPM; PHPSESSID=7s40ubu02ptlvbec3nd91vpb6t")
	req.Header.Set("Host", "20019.ip138.com")
	req.Header.Set("Upgrade-Insecure-Requests", "1")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	bs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	var l = 1024
	if len(bs) < 1024 {
		l = len(bs)
	}

	e, err := detectEncoding(bs[:l])
	if err != nil {
		return "", err
	}

	rd := transform.NewReader(bytes.NewReader(bs), e.NewDecoder())

	bs, err = ioutil.ReadAll(rd)
	if err != nil {
		return "", err
	}

	idx := bytes.Index(bs, start)
	if idx < 0 {
		return "", errors.New("找不到IP位置 [")
	}

	lastIdx := bytes.Index(bs[idx+len(start):], end)
	if lastIdx < 0 {
		return "", errors.New("找不到IP位置 ]")
	}

	return strings.TrimSpace(string(bs[idx+len(start) : idx+len(start)+lastIdx])), nil
}
